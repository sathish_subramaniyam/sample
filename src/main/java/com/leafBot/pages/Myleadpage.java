package com.leafBot.pages;

import org.openqa.selenium.WebElement;

import com.leafBot.testng.api.base.Annotations;

public class Myleadpage extends Annotations{
	
	
	public leadslistpage clickleadtab()
	{
		WebElement createlead = locateElement("xpath", "//a[text()='Leads']");
		click(createlead);  
		return new leadslistpage();
		
	}

}
