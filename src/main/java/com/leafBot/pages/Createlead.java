package com.leafBot.pages;

import org.openqa.selenium.WebElement;

import com.leafBot.testng.api.base.Annotations;

public class Createlead extends Annotations{

	public void entercompname()
	{
		WebElement comp = locateElement("id", "createLeadForm_companyName");
		clearAndType(comp, "Micromen"); 
		
	}
	
	public void enterfname()
	{
		WebElement fname = locateElement("id", "createLeadForm_firstName");
		clearAndType(fname, "Sathish"); 
		
	}
	
	
	public void lastname()
	{
		WebElement lname = locateElement("id", "createLeadForm_lastName");
		clearAndType(lname, "S"); 
		
	}
	
	public void clicksavebutton()
	{
		WebElement sbtn = locateElement("xpath", "//input[@name='submitButton']");
		sbtn.click();
	}
	
	
	
	
	
}
